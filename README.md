# ReactApp
It is a React web application which fetches data from an external API to present data in the form of quotes. 

Hosted on Gitlab Pages for ease. A simple project that utlises Gitlab pipeline to build, "test" & deploy the web app. 


Pipelineproject
├── public
│   ├── favicon.ico
│   ├── index.html
│   ├── logo192.png
│   ├── logo512.png
│   ├── manifest.json
│   └── robots.txt
├── src
│   ├── App.css
│   ├── App.js
│   ├── SocialFollow.js
│   ├── index.js
│   
│   
├── .gititgnore
├── .gitlab-ci.yml
├── package.json
└── README.md


/public
This directory contains assets that will be served directly without additional processing by webpack. index.html provides the entry point for the web app. You will also see a favicon (header icon) and a manifest.json.

/src 
This contains the JavaScript that will be processed by webpack and is the heart of the React app. 

package.json 
This file outlines all the settings for the React app.


.gitlab-ci.yml
The file which contain your CI/CD configuration.



